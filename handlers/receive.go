package handlers

import (
	"../config/config"
)

//questions.getAll
type Request struct {
	session_token Token
	user_name     string
}

//User response to questions
type Response struct {
	session_token Token
	user_name     string
	payload       []AnswerArchetype
}

//Need to register a procedure with AURA

//(reference the config.go file somehow)
//var Procedure

func () RegisterProcedure() {
	configs := &config.Configs{}
	configs.Init()

	cli := client.AuraClient{Realm: configs.PublicRealm.Realm, URL: "localhost:3131", Logger: logger}

	err := cli.Client.Register("question.getAll", handlers.RegistrationHandler, nil)
	if err != nil {
		logger.Panicln("failed to register question.getAll: ", err)
		return err
	}
	err := cli.Client.Register("question.handleResponse", handlers.RegistrationHandler, nil)
	if err != nil {
		logger.Panicln("failed to register question.handleResponse: ", err)
		return err
	}
}
