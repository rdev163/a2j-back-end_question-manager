package main


import (

	"log"
	"os"
	"os/signal"

)

func main(){
	logger:= log.New(os.Stdout, "Question Manager >", log.LstdFlags)

	//Create a comms channel to the OS to receive system events
	//such as shutdown, etc.
	sigChan := make(chan os.Signal, 1)

	//(?) sets up the operating system interrupt as an emittable event
	signal.Notify(sigChan, os.Interrupt)

	select {
		//<- means signal channel will emit a message to "case", which
		//"validates" it, "openingi the channel" and launching the following code:
		case <-sigChan:
			logger.Println("Shutting Down!")
	}


}